# Kubernetes Technical Test

* Author: Axel Herrera <eric.gnome@gmail.com>

## Objetivos

Presentar información detallada de los siguientes tópicos referentes al proyecto:

* Definir los requisitos para la implementación de la solución propuesta.
* Presentar la Arquitectura de la solución.
* Describir a detalle cada uno de los componentes de la solución
* Explicar las interacciones entre los componentes.
* Explicar la operación para CI/CD de la solución.

## Requisitos

Se requiere una solución que ayude a automatizar los procesos de construcción, liberación, y el despliegue de microservicios
desarollados en python, teniendo en cuenta la creación dinámica de consumers en el sistema de la nube,
que tienen comunicación entre sí a través del un sistema de pub/sub para la recepción de mensajes entre los microservicios.

La solución debe considerar la automatización de procesos de CI/CD, Testing, helt checks, autoscaling, Docker y Kubernetes.

### Hardware

El ambientes de Staging, estarán dentro del Hardware la nube de Google Cloud Plataform.

### Entornos

Se contar con un sistema para desplegar los servicios
en un ambiente dedicado a **staging**. Para dichos
ambientes se requiere de las siguientes características.

* **Staging Cluster**
  * Deployer    (g1-small - 1 vCPU, 1,7 GB de memoria)
  * Kubernetes  (3x n1-standard-1 - 1 vCPU, 3,75 GB de memoria)
  * Block Storage (PVC)


* **Gestión de Código**
  * **GitLab** – Gestión de repositorios de código.
  *  Url del proyecto: https://gitlab.com/eric.gnome/entropy-technical-test

  * **Nube GCP**
    * DNS External  (No disponlibe porrol de cuenta).
    * Cloud Load Balancing (No disponible por rol de cuenta).

## Solución

En esta sección se describe la propuesta de solución para implementar el sistema
de despliegue de los microservicios en clusters de contenedores Docker orquestados por Kubernetes en
Google Cloud Plataform (GCP).

Este sistema utiliza el siguite flujo de CI/CD para las distintas fases de construcción y despliegue.

<img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/pipeline.png" width="900">

### Descripción funcional

Se tiene un microservicio llamado "microsvc" el cual está escrito en Python para generar Producers y Consumers de manera dinámica a través de Jenkins, el cual genera un contenedor Docker en el proceso de CI descrito en el diagrama de arriba.

Los producers deployados se adjuntan a un topic o tema: **Entropy-test-%s** donde %s en el número de producer deployado para enviar un mensaje al tema un mensaje

Los consumers a través de un **subscriber** se adjuntan al topic y reciben los mensajes para su procesamiento. Un producer puede tener varios consumers, pero en este ejemplo es un producer y un consumer por Deploy en cada POD del cluster. Este proyecto se encuentra el directorio **microsvc** dentro del proyecto.

Apache Pulsar es un sistema de mensajería pub-sub distribuido de código abierto. Se encuenta deployado dentro del cluster de manera manual, ya que se tuvieron que hacer ajustes para deployar una version micro. Sus playbooks y scripts de instalación se encuentran en el directorio **pulsar** dentro del proyecto.

Se deploya Pulsar-dashboard para la visualización de los mensajes, subscriptores y consumers en los distintos namespaces configurados.

<img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/pub_sub.png">


### Arquitectura

En el siguiente diagrama se muestra la arquitectura completa de la solución.

<img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/arch.png" width="800">

## Estructura de la soclución

Árbol de los componentes:

* **entropy-technical-test**
* **Kubernetes**
  * extramodules
    * playbooks
      - production_issuer.yaml
    * scripts
      - bootstrap.sh
      - create_basic_auth.sh
   * vars
      - allvars
  * **microsvc**
    * playbooks
      - deployment.yaml
      - ingress.yaml
    * scripts
      - deploy.sh
      - docker_build.sh
      - undeploy.sh
    * service      
      - consumer.py
      - producer.py
      - launcher.sh
    - Dockerfile
    - Jenkinsfile
    - package.json
  * **pulsar**
    * playbooks
      - bookie.yaml
      - brooker.yaml
      - cluster-metadata.yaml
      - pulsat-dashboard.yaml
      - storage-class.yaml
      - storage.yaml
      - zookeeper-micro.yaml
    * scripts
      - deploy.sh    
      - undeploy.sh

Definición de archivos.

* **Kubernetes** Contienelos archivos para inicializar el cluster.
* **extramodules**
  - *vars* -> **allvars** Este archivo se copia al path **/var/lib/jenkins/allvars** y se importa los diferentes scripts que se encargan de la construcción, pruebas, y liberación de imagenes docker. Contiene la información de variables de servicios y aplicaciones.
  - *playbooks* -> **production_issuer.yaml** Este archivo genera un Issuer para los certificados del cluster(en este caso no aplica ya que el rol de la cuenta no tiene permisos suficientes).
  - **scripts**
    - **bootstrap.sh** Este proceso instala y configura diferentes componentes requeridos por las bases de
    datos y aplicaciones a desplegar. Así como el secret para pull de GCP registry.
    - **create_basic_auth.sh** Este script crea scretos para una basic-auth en nginx.(nota: mi cuenta no tiene permiso para crear los roles de nginx).
* **microsvc** Este es nuestro componente que será deployado dinamicamente al cluster.
    - **playbooks** Contiene las instrucciones para la construcción y deploy de la imagen de nuestra aplicación al cluster.
  - **scripts** Scripts bash para la generación de archivos de  manifiesto para despliegues de aplicaciones en Kubernetes así como procesos administrativos del mismo.
    - **docker_build.sh** Contiene instrucciones para ejecutar procesos de build, test y
      release(CI). La construcción de la imagen se realiza usando este script.
      El proceso de construcción normalmente es ejecutado desde un Pipeline en Jenkins,
      este pipeline ejecuta la secuencia de stages.
      - **deploy.sh** Este script es usado por Jenkins para el deploy al cluster del          microservicio(CD).
      - **undeploy.sh** Este script es usado por Jenkins para el borrado del microservicio en el cluster.
  - **service** Scripts de la aplicación del microservice.
      - **consumer.py** Proceso que se adjunta a un tema a través de una suscripción y luego recibe mensajes.
      - **producer.py** Proceso que se adjunta un tema y publica mensajes a un agente de Pulsar para su procesamiento.
      - **launcher.sh** Proceso que lanza los componentes anteriores dentro del contenedor.

  - **Dockerfile** Contiene instrucciones para construir y ejecutar el contenedor.          
  - **Jenkinsfile** Contiene instrucciones para construir un Pipeline Declarativo.
  - **package.json** Este archivo contiene los metadatos del microsvc, el metatadato **version** se obtiene de este archivo para realizar el versionado de imagenes Docker.

* **pulsar** Aplicación pub/sub.

## Construcción del Cluster Kubernetes

Para crear un cluster kubernetes en GCP es necesario tener acceso a el
**control panel**.

En el menú **Kubernetes Engine**, haga clic en **Cluster** y vaya a la página de **Create a Cluster**.
El cluster debe parametrizarse con los siguientes datos:

* **Versión Kubernetes**: Por default se selecciona la última estable, actualmente
**1.14.4-do.0** (Septiembre 2019).
* **Datacenter**: Usar aquellos con soporte kubernetes y block storage, actualmente **SFO**.
* **Región**: La región del cluster para nodos master y workers.
* **Capacidad**: Dependerá del ambiente, e.g. los nodos de staging son de menor capacidad
que los nodos de production.

Para crear un cluster, debe agregar por lo menos un pool de nodos con al menos un worker.
Son necesarios los siguientes campos:

* **Node pool name**:
* **Machine type **: En staging se usan nodos standart-cluster y nodos de propósito general.
* **Number nodes**: En staging usar 3 nodos de tipo básico para pool.

## Configuración de Herramientas de gestión

### K-MAJI Orquesta (deployer y tester)

Se instaló el equipo **deployer** en una instancia micro dentro de GCP.

En esta máquina se tienen instaladas las herramientas para hacer los despliegues
de los diferentes componentes del cluster y aplicaciones.

**NOTA:** *Para entrar por SSH a este equipo serán enviadas las credenciales.*

En este servidor, tanto el usuario root como el usuario jenkins tienen configurada
la herramienta **kubectl** para la gestión de los clusters.

Para entrar al portal Jenkins de la máquina k-maji-deployer ingrese al siguiente
URL: [http://<ip_publica>:8080](http://<ip_publica>:8080).

Hay dos vistas(pestañas) para los pipelines para Construcción y Despliegue.

<center><img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/jenkins_view.png"></center>


En el folder llamado **Entropy** también se pueden visualizar los pipelines.


### kubectl y gclod.

 La instalación y configuración de las herramientas kubectl y gcloud, que
  sirven para gestionar los recursos en la nube de GCP, se han configurado manualmente
  siguiendo la guía de GCP.


Verificación del cluster:
```
$ kubectl cluster-info
```

Verificación de nodos:

```
$ kubectl get nodes
NAME                                                STATUS   ROLES    AGE   VERSION
gke-standard-cluster-1-default-pool-ab72de17-2gbk   Ready    <none>   29d   v1.13.10-gke.0
gke-standard-cluster-1-default-pool-ab72de17-w4bn   Ready    <none>   29d   v1.13.10-gke.0
gke-standard-cluster-1-default-pool-ab72de17-zw4v   Ready    <none>   29d   v1.13.10-gke.0
```

## Continuous Integration

### Requisitos para CI

Los procesos aquí descritos están diseñados para ejecutarse desde el servidor
k-maji deployer. En este servidor se  encuentra instalada la herramienta Jenkins, que realiza los procesos de CI.

Durante la instalación de k-maji deployer se genera un archivo de configuración
**.env** que contiene las variables de ambiente del sistema. Este archivo se
encuentra en el directorio **$HOME** del usuario jenkins.

### Ejecución de proceso CI

Cada vez que se hace un merge de un feature a la rama master, se debe ejecutar
un trabajo de Jenkins que realiza un **Checkout** del código desde el servidor
GitLab, despues se debe ejecutar el proceso de construcción del código (**build**),
seguido por pruebas y al final se realiza la liberación del artefacto en el
repositorio central en Dockerhub.

Idealmente GitLab debe ejecutar un webhook (o integración) para ejecutar un
Pipeline de Jenkins de forma automatizada. Esta conexión se realiza a través
de la API de Jenkins.

La propuesta es crear tres ramas dentro del proyecto en Gitlab(https://gitlab.com/eric.gnome/entropy-technical-test):
- development
- master
- production

Se realizan los commit a la rama **develment**. se pueden crear ramas temporales como *hotfix/algun_fin* para después hacer MR.

Realizaremos Merge request(MR) a **master** cuando los componentes pusheados hayan sido testados y en correcto funcionamiento.

En la rama **production** realizaremos Merge request(MR) cuando el ambiente de staging este completo y funcional al 100% para crear una nueva versión segun la semántica de SemVer: https://semver.org/ 2.0.0

**MAJOR.MINOR.PATCH**

*  MAJOR version when you make incompatible API changes,
*  MINOR version when you add functionality in a backwards compatible manner, and
*  PATCH version when you make backwards compatible bug fixes.

Como nota adicional el release del kernel de Linux usa esta semántica.


Para este ejercicio dentro de Jenkins utilizamos la rama **develment**.

### Construcción de Imágenes de Docker

Con el fin de construir las imágenes de contenedores para el microservicio deberá
generar dos archivos en la raíz del repositorio de cada aplicación:

* **Dockerfile:** Contiene instrucciones para construir y ejecutar el contenedor.
* **docker_build.sh:**  Contiene instrucciones para ejecutar procesos de build, test y
release.


## Creación del Pipeline de CI

Dentro del repositorio de la aplicación deberá crear un archivo **Jenkinsfile**
y ubicarlo en la raíz del proyecto. En el siguiente ejemplo se muestra el contenido
que debería tener el Jenkinsfile para crear el pipeline del **microservice**

Cada Pipeline ejecuta los siguientes stages:

* **Checkout:** Descarga el código del de la rama específica del repositorio,
define variables para los procesos de CI de la aplicación y limpia cualquier imagen vieja.
* **Build:** Construye la imagen del contenedor docker en el servidor CI jenkins.
* **Test:** Prueba la imágen del contenedor docker.
* **Release:** Libera la imagen del contenedor docker en el dockerhub registry y
limpia cualquier imagen anteriormente generada.
**IMPORTANTE:** En el stage de Checkout se debe definir el **URL** del repositorio
en GitLab y la rama en la variable **BRANCH**

En las instrucciones del Jenkinsfile se hace mención a un script de shell de nombre
**docker_build.sh** el cual también debe estar en la raíz del repositorio. Dicho script
define las variables usadas para la construcción y liberación de la imágen docker.

Como se puede ver, este script incluye las variables generales del ambiente CI
desde el archivo **/var/lib/jenkins/allvars**. Por cada aplicación se deben definir
variables específicas.

## Pruebas de Imágenes Docker

Para probar los contenedores, se usa el target **test** del sript **docker_build.sh**,
este ejecuta el contenedor localmente con algunos parámetros para exponer la aplicación en un
puerto local, el cual es redireccionado al puerto del contenedor.

El proceso de pruebas normalmente es ejecutado desde un Pipeline en Jenkins, este
pipeline ejecuta la secuencia de stages.

## Liberación de Imágenes Docker

Cuando se ejecuta un proceso de liberación de imagenes, Jenkins usa las credenciales
de gclod para iniciar sesión en GCP Registry y subir la imagen.

Para liberar las imágenes use el target **release** del script **docker_build.sh**,

Al ejecutarse automaticamente inicia sesión en registry y hace push a los
respectivos repositorios.

## Etiquetado y Versionado de imagenes.

Para establecer los metadatos de version y etiquetadode la imagen dentro de cada proyecto
de propone crear un archivo **package.json** en el cual se establece la versión, nombre, licencia
y descripción del proyecto. Este archivo será leído por el **docker_build.sh** para construir
la imagen con la información del paquete.

<center><img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/ppl.png"></center>


## Despliegues y Servicios de Aplicaciones

### Objetivos

En esta sección describimos los diferentes procesos y herramientas que usamos para
realizar el despliegue de las herramientas iniciales.

### Despliegue bootstrap

Bootstrap es el proceso a ejecutar después de haber construído el cluster kubernetes
y requiere que esten configuradas todas las conexiones de administración.

Este proceso instala y configura diferentes componentes requeridos por las bases de
datos y aplicaciones a desplegar. Las tareas principales que ejecuta son:

* **generateSecret:** Genera secret para hacer pull de GCP registry.
* **initHelm:** Instala e inicializa Helm.
* **configHelm:** Configura Helm.
* **installCertManager:** Instala CertManager.
* **installProductionIssuer:** Instala Letsencrypt issuer.
* **installNginx:** Instala nginx ingress controller.

El despliegue del bootstrap se hace a través de los manifiestos incluídos en el
proyecto **entropy-technical-test**, dentro de la carpeta **kubernetes/extramodules**.

#### Ejecución

Acceda al repositorio de **entropy-technical-test** local:


```
$ cd entropy-technical-test

```

Ahora cambie al directorio kubernetes y luego a extramodules:


```
$ cd kubernetes/extramodules

```

Ejecute el script **scripts/deploy.sh** para lanzar el despliegue:


```
$ bash scripts/bootstrap.sh

```
**NOTA:** Para esta versión no fue posible instalar NGINX ni HELM debido a los permisos de la cuenta.


## Despliegue del microsvc al cluster.

En esta sección describimos los diferentes procesos para
realizar el despliegue de los microservicios dentro de Kubernetes.

### Creación playbooks y scripts

En el repositorio del proyecto **entropy-technical-test** dentro del directorio **kubernetes** tenemos el directorio de nuestra aplicación **microsvc** con la estructura de directorios playbooks y scripts necesarios para el despliegue

El template **playbooks/deployment.yaml**, define el **Deployment** y las configuraciones que tendrá el POD. Tenemos las siguientes cadenas que seran sustituidas cuando Jenkis ejecute el scritp **deploy.sh**:

* **APP_NAME** Nombre de la aplicación, definido en **allvars**.
* **APP_CONTAINER_PORT** Puerto del contenedor, definido en **allvars**.
* **APP_DOCKER_REPO** Nombre del repo y de la imagen docker, definido en **allvars**.
* **num_consumers** Número de microsvcs a deployar(Leído desde Jenkins).
* **APP_LIMIT_MEM** Limite de memoria(Leído desde Jenkins).
* **APP_LIMIT_CPU** Limite de CPU prara el POD(Leído desde Jenkins).
* **APP_REQ_MEM** Request de Memoriapara el POD(Leído desde Jenkins).
* **APP_REQ_CPU** Request de CPU para el POD(Leído desde Jenkins).
* **SECRET_NAME** Nombre del secret Secret para hacer pull de GCP registry.


Los valores de las variables que usaremos para el despliegue se definen en el archivo **../extramodules/vars/allvars**.

## Valores Dinámicos.

### CPU y Memoria

Las variables dinámicas van a ser pasadas a través de Jenkins y seteadas en nuestro YAML **deployment.yaml**.

Al ejecutar el trabajo entro de Jenkins en la vista **01-STAGING**, encontrará el trabajo de **Deploy-Microservice-STG**. Al construir el Job deberá setear las variables de los recursos y límites. Aquí debe tener en cuenta para esta parametrización el tamaño y recursos del pool de nodos del cluster.

<center><img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/buildmicroservice.png"></center>

Y por último probemos desde un navegador acceder al URL de [toloco](https://beta.todosloscontratos.mx/#top).

## Pruebas Readiness Y Liveness al POD.
Ya que nuestro microservicio no cuenta con requests hacia el protocolo http, como tests se definió leer el archivo de log **salida.log** de los eventos del consumer y del producer.

### Readiness
 Si es posible leer el archivo log, la aplicación puede servir el tráfico.

```
  readinessProbe: # If the condition inside readiness probe passes, only then our application can serve traffic.
    exec:
      command:
        - cat
        - /salida.log # This file is the log of our pub/sub script.
    initialDelaySeconds: 10 # delay before performing the first probe.
    periodSeconds: 15 # perform a probe every 15 seconds.
```

### Liveness

Revisa que el container no quede inaccesible por algún proceso colgado, al poder leer el archivo log nos indica que está vivo.


```
livenessProbe: # Liveness probe checks the container health.
  exec:
    command:
      - cat
      - salida.log # This file is the log of our pub/sub script.
  initialDelaySeconds: 3 # delay before performing the first probe.
  periodSeconds: 15 # perform a probe every 15 seconds.
```

## Autoscaling

El autoescalado de nustro deploy esta configurado en el script **deploy.sh** del microsvc.


```  
kubectl autoscale deployment $APP_NAME-$i --cpu-percent=50 --min=1 --max=3
```

Para revisar el estatus del autoscaling ejecute:

```  
$ kubectl get hpa
NAME              REFERENCE                    TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
microsvc-test-1   Deployment/microsvc-test-1   4%/50%    1         3         1          4h4m
microsvc-test-2   Deployment/microsvc-test-2   4%/50%    1         3         1          4h4m
```

El Pod al consumir el 50% de CPU, autoescala a máximo 3 pods para repartir la carga. Estos valores deben ser seteados teniendo en cuenta las capaicades del pool de nodos del cluster, ya que si no se cuenta con los recursos disponibles noserá posible el autoscaling del Pod.


## Visualización de los consumers.
Para ver los Pods deployados ejecute:

```
$ kubectl get pod -o wide | grep microsvc

microsvc-test-1-645cc6c487-97qhh     1/1     Running   0          4h2m   10.8.0.71    gke-standard-cluster-1-default-pool-ab72de17-w4bn   <none>           <none>
microsvc-test-2-666c568845-b9wg7     1/1     Running   0          4h2m   10.8.1.31    gke-standard-cluster-1-default-pool-ab72de17-2gbk   <none>           <none>
```

Para visualizar el tablero de Pulsar a través de la aplicación web que permite a los usuarios monitorear las estadísticas actuales de todos los temas en forma de tabla.

Ejecute en una máquina fuera de GCP con kubectl y gclod bien configurados el comando para port-forward:

```
$ kubectl port-forward svc/pulsar-dashboard 9090:80
Forwarding from 127.0.0.1:9090 -> 80
Forwarding from [::1]:9090 -> 80
Handling connection for 9090
```

Ahora podemos accesar al dashboard de pulsar en **http//localhost:9090**


<center><img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/pulsar.png"></center>


### Administración de mensajes con pulsar-admin
Para borrar los mensajes creados a partir del deployment del microscv ejecute:

```
$ alias pulsar-admin='kubectl exec pulsar-admin -it -- bin/pulsar-admin'
$ pulsar-admin topics delete persistent://public/functions/Entropy-test-%s
```

Entropy-test-%s es el Topic, y %s debe ser sustituido por el número que le corresponde.


### Usuarios

Los usuarios que se verán impactados por la arquitectura propuesta se enumeran a
continuación.

* **Internal**
  * Test users

* **Public**
  * Developers
  * Operadores

## Imagen del Proceso


<center><img src="https://gitlab.com/eric.gnome/entropy-technical-test/raw/development/docs_files/pipeline_2.png"></center>


# Notas adicionales

La segunda fase sería el deploy de Mongo, creación de Ingress con uso de nginx y la documentación deployada en un contenedor para visualización en linea usando basic-auth.
Para el almacenamiento de contraseñas de crean objetos de tipo secret. Y en los deplyment.yaml se leen los secretos para no exponer las contraseñas.

El siguiente error se obtuvo al tratar de crear los **clusterrolebindings** para el uso de los ingress y de Heml.

```
Error from server (Forbidden): clusterrolebindings.rbac.authorization.k8s.io is forbidden: User "eric.gnome@gmail.com" cannot create resource "clusterrolebindings" in API group "rbac.authorization.k8s.io" at the cluster scope: Required "container.clusterRoleBindings.create" permission.
```

Ejemplo de creación de secret para Bases de Datos:

1. Crear un yaml con la clase **Secret**
2.  Crear un archivo con el contenido:

  ```
  echo
  "mongodb://user:pass@mongo-0.mongo.default.svc.cluster.local,mongo-1.mongo.default.svc.cluster.local:27017/my_db?replicaSet=MainRepSet"
  >> secrets/MONGODB_URIsecrets/MONGODB_URI

 ```

 3. Crear el secret en el cluster:

 ```
 kubectl apply -f secrets/MONGODB_URI
 ```

 2) En el deployment:
 Editar la variable para que se tome desde el secret

```
 - name: MONGODB_URI
   valueFrom:
     secretKeyRef:
       key: MONGODB_URI
       name: mongodb-uri
 ```
