#!/bin/bash

# This script clean previous cluster microservices deployment(s).
# Author: Axel Herrera <eric.gnome@gmail.com>

. ../extramodules/vars/allvars

undeploy() {
  cmd=$(kubectl get deployment | grep $APP_NAME | awk '{print $1}')
  if [[ "$cmd" != "" ]]; then
    for i in $cmd; do
      kubectl delete deployment $i;
      kubectl delete hpa $i;
    done
  fi
  sleep 20;
}

# main
undeploy
