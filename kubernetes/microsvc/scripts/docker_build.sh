#!/bin/bash

## This script is used by Jenkins pipiline(Jenkinsfile) for CI.
## Author: Axel Herrera <eric.gnome@gmail.com>

[[ ! -f "$HOME/allvars" ]] && { echo -e "[ERROR]: No se encontro allvars deployado"; exit -1; }

echo $(pwd)

source $HOME/allvars
VERSION=$(cat package.json | jq -r .version)
REPO=${DOCKER_REPO}/${APP_NAME}:${VERSION}
APP=${APP_NAME}:${VERSION}

build() {
	echo -e " ==> Building ${APP} image."
	docker build -t ${APP} .
	echo -e "Listing ${APP} image."
	docker images
}

test() {
	echo -e " ==> Run ${APP} image."
	docker run --name ${APP_NAME} --env num_consumers="1" -d ${APP}  &
	sleep 5
	docker logs ${APP_NAME}
}

release() {
	echo -e " ==> Release ${REPO} image to GCP registry."
	docker tag ${APP} ${REPO}
	gcloud docker -- push ${REPO}
}

clean() {
	echo -e "Cleaning local build environment."
	docker stop ${APP_NAME} 2>/dev/null; true
	docker rm ${APP_NAME}  2>/dev/null; true
	docker rmi ${REPO} 2>/dev/null; true
	docker rmi ${APP} 2>/dev/null; true
}

help() {
	echo -e ""
	echo -e "Please use bash $0 <target> where <target> is one of"
	echo -e "  build		Builds the docker image."
	echo -e "  test		Tests image."
	echo -e "  release	Releases image."
	echo -e "  clean		Cleans local images."
}

if [[ "$1" == "build" ]]; then build;
elif [[ "$1" == "test" ]]; then test;
elif [[ "$1" == "release" ]];then release;
elif [[ "$1" == "clean" ]]; then clean;
elif [[ "$1" == "help" ]];then help;
else
	help
	exit -1
fi
