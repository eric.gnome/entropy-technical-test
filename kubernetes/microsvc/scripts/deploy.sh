#!/bin/bash

# This script deploys the microservice(s) to the kubernetes cluster.
# It deploys a dynamical number kubernetes Deployment class within num_consumers
# var given by Jenkins.
# Then set the Deploymen.yaml vars and apply it to the cluster.
# Author: Axel Herrera <eric.gnome@gmail.com>

. ../extramodules/vars/allvars

validateConsumersNumber() {
  [[ "$num_consumers" -gt "$CONSUMERS_LIMIT" ]] && { echo "[Error]: Too much consumers"; exit -1; }
}

cleanDeploy() {
   # This script clean previous cluster microservices deployment(s).
   bash scripts/undeploy.sh
}

deploy() {
  for i in `seq 1 $num_consumers`; do
    # Create tmp yaml templates.
    cp -a playbooks/deployment.yaml playbooks/deployment-$i.yaml
    # Replace vars on template.
    sed -i \
      -e "s^{{APP_NAME}}^$APP_NAME-$i^g" \
      -e "s^{{APP_CONTAINER_PORT}}^$APP_CONTAINER_PORT^g" \
      -e "s^{{APP_DOCKER_REPO}}^$APP_DOCKER_REPO^g" \
      -e "s^{{NUM_CONSUMERS}}^$num_consumers^g" \
      -e "s^{{APP_LIMIT_MEM}}^$APP_LIMIT_MEM^g" \
      -e "s^{{APP_LIMIT_CPU}}^$APP_LIMIT_CPU^g" \
      -e "s^{{APP_REQ_MEM}}^$APP_REQ_MEM^g" \
      -e "s^{{APP_REQ_CPU}}^$APP_REQ_CPU^g" \
      -e "s^{{SECRET_NAME}}^$SECRET_NAME^g" \
      -e "s^{{CLUSTER_ISSUER}}^$CLUSTER_ISSUER^g" \
    playbooks/deployment-$i.yaml
    # Deploy the object.
    kubectl apply -f playbooks/deployment-$i.yaml
    # Create Horizontal Pod Autoscaler
    # Autoscaler that maintains between 1 and 3 replicas of the Pods controlled by the microsvc deployment we created.
    # CPU utilization across all Pods of 50%
    kubectl autoscale deployment $APP_NAME-$i --cpu-percent=50 --min=1 --max=3
    # Clean tmp yaml templates.
    rm playbooks/deployment-$i.yaml
  done
  printf "\n==> DEPLOY STATS:\n"
  printf "==> microservicios deployados: $num_consumers\n"
  printf "==> POD port-forward: kubectl port-forward svc/pulsar-dashboard 9090:80\n"
  printf "==> URL: http://localhost:9090\n\n"
}

# main
validateConsumersNumber
cleanDeploy
deploy

exit 0
