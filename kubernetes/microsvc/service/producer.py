#!/usr/bin/env python

'''
A producer is a process that attaches to a topic and publishes
messages to a Pulsar broker for processing by the consumer.
Pulsar client has been deployed into the kubernetes cluster(pub-sub system).

Author: Axel Herrera <eric.gnome@gmail.com>
'''

import pulsar
import logging
import sys

class Producer(object):
    def __init__(self, topic):
        logging.basicConfig(level = logging.INFO, filename = "salida.log")
        pulsar_uri = "pulsar://10.8.2.234:6650" # kubermetes svc nodeip
        client = pulsar.Client(pulsar_uri)
        self.producer = client.create_producer('persistent://public/functions/%s' % topic)

    def send(self, num):
        #for i in range(1, num+1):
        msg = 'Hello from consumer%s' % str(consumerNumber)
        msg_encode = msg.encode('utf8')
        try:
            logging.info(msg_encode)
            self.producer.send(msg_encode)
        except Exception as e:
            logging.error(e)
        #self.producer.flush()
        self.producer.close()

if __name__=="__main__":
    consumerNumber = sys.argv[1]
    p = Producer('Entropy-test-%s' % str(consumerNumber))
    p.send(int(sys.argv[1]))
    sys.exit(0)
