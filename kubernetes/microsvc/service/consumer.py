#!/usr/bin/env python

'''
This script creates dynamic consumers, this is a process that attaches
to a topic via a subscription and then receives messages.
Pulsar client has been deployed into the kubernetes cluster(pub-sub system).

Author: Axel Herrera <eric.gnome@gmail.com>
'''

import pulsar
import logging
import os
import time
import sys

class Consumer(object):
    def __init__(self, topic):
        logging.basicConfig(level = logging.INFO, filename = "salida.log")
        self.topic = topic
        self.pulsar_uri = "pulsar://10.8.2.234:6650" # kubermetes svc nodeip
        self.client = pulsar.Client(self.pulsar_uri)

    def subscribe(self, name):
        '''Consumer subscribes to the topic, using exclusive mode'''
        try:
            consumer = self.client.subscribe(
                    'persistent://public/functions/%s' % self.topic,
                    '%s' % name)
            while True:
                msg = consumer.receive()
                msg_format = "Received message '{0}' id='{1}'".format( msg.data().decode('utf-8'), msg.message_id() )
                logging.info(msg_format)
                print(msg_format)
                consumer.acknowledge(msg)
        except Exception as e:
            logging.info('Este consumer ya esa en uso: %s' % name)
            print('Este consumer ya esa en uso: %s' % name)
        #self.client.close()

if __name__=="__main__":
    consumerNumber = str(sys.argv[1])
    consumer = Consumer('Entropy-test-%s' % consumerNumber)
    consumer.subscribe('Consumer-%s' % consumerNumber)
