#!/bin/bash

# This script creates dynamic consumers, this is a process that attaches
# to a topic via a subscription and then receives messages.
# Pulsar client has been deployed into the kubernetes cluster(pub-sub system).
# Author: Axel Herrera <eric.gnome@gmail.com>

[[ "$num_consumers" -gt "5" ]] && { echo "[Error]: Too much consumers"; exit -1; }

for i in `seq 1 $num_consumers`; do
  python consumer.py "$i" &
  sleep 3;
  python producer.py "$i" &
  echo -ne 'Finishing.\n'
done

exit 0
