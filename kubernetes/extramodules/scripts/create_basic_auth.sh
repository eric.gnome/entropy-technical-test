#!/bin/bash

# this script creates a basic auth for using into an ingress object.
# Author: Axel Herrera <eric.gnome@gmail.com>

FILE=/tmp/auth
SECRET=$1
PWD=$2
CONTEXT=$3

htpasswd -b -c $FILE $SECRET $PWD
kubectl create secret generic $SECRET --from-file $FILE -n $CONTEXT
kubectl get secret $SECRET -o yaml
rm $FILE
