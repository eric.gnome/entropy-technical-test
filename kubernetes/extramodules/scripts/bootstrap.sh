#!/bin/bash

# Kubernetes cluster boostrap file for GCP
# Author: Axel Herrera <eric.gnome@gmail.com>

source vars/allvars

getSecret() {
  printf "\n==> Getting $SECRET_NAME\n"
  kubectl get secret $SECRET_NAME --output=yaml 2>/dev/null
}

generateSecret() {
  printf "\n==> Generating GCP Registry secret: $SECRET_NAME\n"
  kubectl create secret docker-registry $SECRET_NAME \
  --docker-server=$DOCKER_SERVER \
  --docker-username=$DOCKER_USER \
  --docker-password="$(cat $DOCKER_PWD)" \
  --docker-email=$DOCKER_MAIL
}

createRolBinding() {
  printf "\n==> Create clusterrolebinding\n"
  kubectl create clusterrolebinding cluster-admin \
  --clusterrole=cluster-admin \
  --user=$DOCKER_MAIL
}

initHelm() {
  printf "\n==> Init helm and Tiller\n"
  kubectl -n kube-system create serviceaccount tiller
  kubectl create clusterrolebinding tiller \
  --clusterrole cluster-admin  \
  --serviceaccount=kube-system:tiller
  helm init --service-account tiller
  sleep 10
  helm repo update
}

installNginx() {
  printf "\n==> Install nginx-ingress\n"
  CMD=$(helm ls --all nginx-ingress | wc -l)
  if [[ "$CMD" == 0 ]]; then
    helm install stable/nginx-ingress --name nginx-ingress \
    --set controller.publishService.enabled=true \
    --set controller.extraArgs.enable-ssl-passthrough=""
  else
    printf "==> Nginx-ingress already installed \n"
  fi
}

configHelm() {
  printf "\n==> Add cert-manager CRDs \n"
  kubectl apply -f ${CERT_MANAGER_CRDS_URL}
}

installCertManager() {
  printf "\n==> Label cert-manager namespace without validation \n"
  CMD=$(kubectl get namespace cert-manager | wc -l)
  if [[ "$CMD" == 0 ]]; then
    kubectl label namespace cert-manager certmanager.k8s.io/disable-validation="true"
  else
    printf "==> cert-manager namespace already exists \n"
  fi

  printf "\n==> Install cert-manager repo and package \n"
  CMD=$(helm ls --all cert-manager | wc -l)
  if [[ "$CMD" == 0 ]]; then
    helm repo add jetstack ${CERT_MANAGER_REPO}
    helm install --name cert-manager --namespace cert-manager jetstack/cert-manager
    sleep 5
  else
    printf "==> cert-manager already installed \n"
  fi
}

installProductionIssuer() {
  printf "\n==> Install Letsencrypt production issuer\n"
  CMD=$(kubectl get clusterissuer letsencrypt-prod | wc -l)
  if [[ "$CMD" == 0 ]]; then
    kubectl create -f playbooks/production_issuer.yaml
  else
    printf "==> cert-manager letsencrypt production issuer already installed \n"
  fi
}

# main
getSecret
[[ $? == 1 ]] && generateSecret
createRolBinding
initHelm
configHelm
installCertManager
installProductionIssuer
installNginx

exit 0
