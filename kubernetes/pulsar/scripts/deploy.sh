#!/bin/bash

# Minimal Pulsar setup for a kubernetes cluster.
# For re-deploy remove /tmp/disks/ssd0 on node labeled node has-local-fast-storage=yes

deploy() {
  kubectl apply -f playbooks/storage-class.yaml
  #kubectl apply -f playbooks/storage.yaml
  kubectl apply -f playbooks/zookeeper_micro.yaml
  kubectl apply -f playbooks/cluster-metadata.yaml
  kubectl apply -f playbooks/bookie.yaml
  kubectl apply -f playbooks/broker.yaml
  kubectl apply -f playbooks/pulsar-dashboard.yaml
}

# main
deploy

exit 0
