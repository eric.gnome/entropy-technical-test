#!/bin/bash

undeploy() {
  kubectl delete -f playbooks/zookeeper_micro.yaml
  kubectl delete -f playbooks/cluster-metadata.yaml
  kubectl delete -f playbooks/bookie.yaml
  kubectl delete -f playbooks/broker.yaml
  kubectl delete -f playbooks/pulsar-dashboard.yaml
  kubectl delete pvc datadir-zk-0
}

# main
undeploy

exit 0
